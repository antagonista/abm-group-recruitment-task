<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Person Routes
Route::get('person/getAll', 'PersonController@getAll');
Route::get('person/get/{id}', 'PersonController@get')->where('id', '[0-9]+');
Route::post('person/add', 'PersonController@add');
Route::put('person/edit', 'PersonController@edit');
Route::delete('person/delete/{id}', 'PersonController@delete')->where('id', '[0-9]+');

// Equipment Routes
Route::post('equipment/add', 'EquipmentController@add');
Route::get('equipment/getAll', 'EquipmentController@getAll');
Route::get('equipment/get/{id}', 'EquipmentController@get')->where('id', '[0-9]+');
Route::put('equipment/edit', 'EquipmentController@edit');
Route::delete('equipment/delete/{id}', 'EquipmentController@delete')->where('id', '[0-9]+');
Route::get('equipment/getWorkplaces', 'WorkplaceController@getForEquipmentForm');

// Workplace Routes
Route::post('workplace/add', 'WorkplaceController@add');
Route::get('workplace/getAll', 'WorkplaceController@getAll');
Route::get('workplace/get/{id}', 'WorkplaceController@get')->where('id', '[0-9]+');
Route::put('workplace/edit', 'WorkplaceController@edit');
Route::delete('workplace/delete/{id}', 'WorkplaceController@delete')->where('id', '[0-9]+');

// Reservation Routes
Route::post('reservation/add', 'ReservationController@add');
Route::get('reservation/getAll', 'ReservationController@getAll');
Route::get('reservation/get/{id}', 'ReservationController@get')->where('id', '[0-9]+');
Route::put('reservation/edit', 'ReservationController@edit');
Route::delete('reservation/delete/{id}', 'ReservationController@delete')->where('id', '[0-9]+');
Route::get('reservation/getDataForm', 'ReservationController@getDataForm');
