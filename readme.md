# ABM Group - Recruitment Task

## Info
* Laravel 7
* Vue.js 2.0 + Vuetify
* Rest API
 
## Install

* `git clone git@gitlab.com:antagonista/abm-group-recruitment-task.git`
* `composer install`
* `npm install`
* `cp .env.example .env`
* edit .env

DB_CONNECTION=mysql - (or -> https://laravel.com/docs/7.x/database#configuration)
DB_HOST=host_database
DB_PORT=port_database
DB_DATABASE=name_database
DB_USERNAME=user_database
DB_PASSWORD=password_database

* `php artisan key:generate`
* `php artisan config:cache`
* `php artisan migrate`
* `npm run dev`
* `php artisan serve`

# Docker
https://hub.docker.com/r/antagonista/abm_group-recruitment_task

* `docker pull antagonista/abm_group-recruitment_task`
