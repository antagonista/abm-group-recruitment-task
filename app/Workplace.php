<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workplace extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workplace';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mark',
        'description'
    ];
}
