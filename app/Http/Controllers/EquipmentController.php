<?php

namespace App\Http\Controllers;

use App\Http\Requests\Equipment as ValidationEquipment;
use App\Http\Requests\EquipmentEdit as ValidationEquipmentEdit;
use App\Equipment as ModelEquipment;
use App\Workplace as ModelWorkplace;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EquipmentController extends Controller
{

    /**
     * Add new Equipment
     *
     * @param ValidationEquipment $request
     * @return JsonResponse
     */
    public function add(ValidationEquipment $request)
    {

        $dataRequest = $request->all();
        $person = new ModelEquipment($request->all());

        if ($person->save()) {
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => ['Błąd przy zapisywaniu']], 422);
        }
    }

    /**
     * Edit Equipment
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(ValidationEquipmentEdit $request)
    {

        $requestData = $request->all();

        $equipment = ModelEquipment::where('id', $requestData['id']);

        if ($equipment !== null and $equipment->count() === 1) {
            $personActually = $equipment->get()->toArray();

            if ($personActually[0]['mark'] !== $requestData['mark']) {
                // Checking unique mark when change

                $count = ModelEquipment::where('mark', $requestData['mark'])
                    ->where('id', '!=', $personActually[0]['id'])
                    ->count();

                if ($count !== 0) {
                    return response()->json(['messages' => ['Podane oznaczenie jest juz używane']], 422);
                }
            }

            $equipment->update([
                'id_workplace' => $requestData['id_workplace'],
                'type' => $requestData['type'],
                'model' => $requestData['model'],
                'mark' => $requestData['mark'],
                'purchase_year' => $requestData['purchase_year'],
                'value' => $requestData['value'],
                'description' => $requestData['description']
            ]);
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => ['Błąd : nie można edytować danego wyposażenia']], 422);
        }
    }

    /**
     * Get one Equipment
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id)
    {

        $person = ModelEquipment::where('id', $id)
            ->get([
                'id_workplace',
                'type',
                'model',
                'mark',
                'purchase_year',
                'value',
                'description'
            ]);

        if ($person !== null and $person->count() === 1) {
            return  response()->json($person, 200);
        } else {
            return response()->json(['messages' => ['Błąd : Dane wyposażenie nie istnieje']], 422);
        }
    }

    /**
     * Get all Equipments
     *
     * @return JsonResponse
     */
    public function getAll()
    {

        $allEquipment = ModelEquipment::all();

        $responseArray = [];
        foreach ($allEquipment->all() as $equipment) {
            // Get workstation name
            $workstationName = '-';

            if ($equipment->id_workplace) {
                $workstation = ModelWorkplace::where('id', $equipment->id_workplace)
                    ->get(['mark'])
                    ->first();
                $workstationName = $workstation->mark;
            }

            $responseArray[] = [
                'id' => $equipment->id,
                'workplace' => $workstationName,
                'type' => $equipment->type,
                'model' => $equipment->model,
                'mark' => $equipment->mark,
                'purchase_year' => $equipment->purchase_year,
                'value' => $equipment->value,
                'description' => $equipment->description,
            ];
        }

        return response()->json($responseArray, 200);
    }

    /**
     * Delete Equipment
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {

        $equipment = ModelEquipment::where('id', $id);
        if ($equipment !== null and $equipment->count() === 1) {
            // Equipment exist
            if ($equipment->delete()) {
                return response()->json(null, 200);
            } else {
                return response()->json(['messages' => ['Błąd usuwania wyposażenia']], 422);
            }
        } else {
            return response()->json(['messages' => ['Błąd usuwania wyposażenia : dane wyposażenie nie istnieje']], 422);
        }
    }
}
