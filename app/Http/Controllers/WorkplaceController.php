<?php

namespace App\Http\Controllers;

use App\Http\Requests\Workplace as ValidationWorkplace;
use App\Http\Requests\WorkplaceEdit as ValidationWorkplaceEdit;
use App\Workplace as ModelWorkplace;
use App\Equipment as ModelEquipment;
use App\Reservation as ModelReservation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WorkplaceController extends Controller
{
    /**
     * Add new Workplace
     *
     * @param ValidationWorkplace $request
     * @return JsonResponse
     */
    public function add(ValidationWorkplace $request)
    {

        $workplace = new ModelWorkplace($request->all());

        if ($workplace->save()) {
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => ['Błąd przy zapisywaniu']], 422);
        }
    }

    /**
     * Edit Workplace
     *
     * @param ValidationWorkplaceEdit $request
     * @return JsonResponse
     */
    public function edit(ValidationWorkplaceEdit $request)
    {

        $requestData = $request->all();

        $workplace = ModelWorkplace::where('id', $requestData['id']);

        if ($workplace !== null and $workplace->count() === 1) {
            $workplaceActually = $workplace->get()->toArray();

            if ($workplaceActually[0]['mark'] !== $requestData['mark']) {
                // Checking unique mark when change

                $count = ModelWorkplace::where('mark', $requestData['mark'])
                    ->where('id', '!=', $workplaceActually[0]['id'])
                    ->count();

                if ($count !== 0) {
                    return response()->json(['messages' => ['Podane oznaczenie jest juz używane']], 422);
                }
            }

            $workplace->update([
                'mark' => $requestData['mark'],
                'description' => $requestData['description']
            ]);
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => ['Błąd : nie można edytować danego miejsca pracy']], 422);
        }
    }

    /**
     * Get one Workplace
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id)
    {

        $workplace = ModelWorkplace::where('id', $id)
            ->get([
                'id',
                'mark',
                'description'
            ]);

        if ($workplace !== null and $workplace->count() === 1) {
            return  response()->json($workplace, 200);
        } else {
            return response()->json(['messages' => ['Błąd : Dane miejsce pracy nie istnieje']], 422);
        }
    }

    /**
     * Get all Workplaces
     *
     * @return JsonResponse
     */
    public function getAll()
    {

        $allWorkplaces = ModelWorkplace::all([
            'id',
            'mark',
            'description'
        ])->toArray();

        // Add equipments to workplaces
        $changedWorkplaces = [];

        foreach ($allWorkplaces as $workplace) {
            $equipments = ModelEquipment::where('id_workplace', $workplace['id'])
                ->get(['mark'])
                ->toArray();
            $tempArray = $workplace;
            $tempArray['equipments'] = $equipments;
            $changedWorkplaces[] = $tempArray;
        }

        return response()->json($changedWorkplaces, 200);
    }

    /**
     * Get data Workplaces for Equipment form
     *
     * @return JsonResponse
     */
    public function getForEquipmentForm()
    {

        $allPersons = ModelWorkplace::all([
            'id',
            'mark'
        ]);

        return response()->json($allPersons->all(), 200);
    }

    /**
     * Delete Workplace
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {

        $workplace = ModelWorkplace::where('id', $id);
        if ($workplace !== null and $workplace->count() === 1) {
            // Equipment exist
            if ($workplace->delete()) {
                // Delete related reservations
                ModelReservation::where('id_workplace', $id)->delete();

                // Deselect related equipment
                ModelEquipment::where('id_workplace', $id)->update(['id_workplace' => null]);

                return response()->json(null, 200);
            } else {
                return response()->json(['messages' => ['Błąd usuwania miejsca pracy']], 422);
            }
        } else {
            return response()->json(
                ['messages' => ['Błąd usuwania miejsca pracy : dane miejsce pracy nie istnieje']
                ],
                422
            );
        }
    }
}
