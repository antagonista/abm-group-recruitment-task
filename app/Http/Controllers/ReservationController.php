<?php

namespace App\Http\Controllers;

use App\Http\Requests\Reservation as ValidationReservation;
use App\Reservation as ModelReservation;
use App\Workplace as ModelWorkplace;
use App\Equipment as ModelEquipment;
use App\Person as ModelPerson;
use DateTime;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    /**
     * Add new Reservation
     *
     * @param ValidationReservation $request
     * @return JsonResponse
     * @throws Exception
     */
    public function add(ValidationReservation $request)
    {

        if ($this->checkModelsExist($request->get('id_person'), $request->get('id_workplace'))) {
            $date_start = $this->changeDateFormat($request->get('datetime_start'));
            $date_end = $this->changeDateFormat($request->get('datetime_end'));

            if ($this->checkDateExpired($date_start, $date_end)) {
                if ($this->checkDateLogic($date_start, $date_end)) {
                    if ($this->checkFreeDateWorkplace($request->get('id_workplace'), $date_start, $date_end)) {
                        if ($this->checkFreeDataPerson($request->get('id_person'), $date_start, $date_end)) {
                            $reservation = new ModelReservation($request->all());

                            $reservation->datetime_start = $date_start;
                            $reservation->datetime_end = $date_end;

                            if ($reservation->save()) {
                                return response()->json(null, 200);
                            } else {
                                return response()->json(['messages' => ['Błąd przy zapisywaniu']], 422);
                            }
                        } else {
                            return response()->json(
                                ['messages' => ['Ta osoba rezerwuje już miejsce w tym terminie']
                                ],
                                422
                            );
                        }
                    } else {
                        return response()->json(
                            ['messages' => ['Miejsce pracy w wybranym terminie jest już zajęte']
                            ],
                            422
                        );
                    }
                } else {
                    return response()->json(
                        ['messages' => ['Data początkowa nie może być większa od daty końcowej']
                        ],
                        422
                    );
                }
            } else {
                return response()->json(
                    ['messages' => ['Podana data już się przedawniła']
                    ],
                    422
                );
            }
        } else {
            return response()->json(
                ['messages' => ['Podana Osoba lub Miejsce pracy nie istnieje']
                ],
                422
            );
        }
    }

    /**
     * Change data to 'Y-m-d h:i' format
     *
     * @param $date
     * @return string
     * @throws Exception
     */
    private function changeDateFormat($date)
    {
        $date = new DateTime($date);
        return $date->format('Y-m-d h:i');
    }

    /**
     * Check validates dependencies and requirements
     *
     * @param int $idPerson
     * @param int $idWorkplace
     * @return bool|JsonResponse
     */
    private function checkModelsExist(int $idPerson, int $idWorkplace)
    {

        $person = ModelPerson::where('id', $idPerson)->count();
        $workplace = ModelWorkplace::where('id', $idWorkplace)->count();

        if ($person === 1 and $workplace ===1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check dates are not expired
     *
     * @param $date_start
     * @param $date_end
     * @return bool
     */
    private function checkDateExpired($date_start, $date_end)
    {
        //check dates are not expired
        $dateNow = date('Y-m-d h:i');

        if ($date_start < $dateNow or $date_end < $dateNow
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check date start > date end
     *
     * @param $date_start
     * @param $date_end
     * @return bool
     */
    private function checkDateLogic($date_start, $date_end)
    {
        if ($date_start >= $date_end) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check free data for Workplace
     *
     * @param int $id_workplace
     * @param $date_start
     * @param $date_end
     * @param null $idReservation
     * @return bool|JsonResponse
     */
    private function checkFreeDateWorkplace(int $id_workplace, $date_start, $date_end, $idReservation = null)
    {

        if ($idReservation !== null) {
            $countFreeDate = DB::select(
                'SELECT count(id) as count FROM reservation WHERE id_workplace = ? AND id != ? AND ((? BETWEEN datetime_start AND datetime_end) OR (? BETWEEN datetime_start AND datetime_end) OR (? < datetime_start and ? > datetime_end))',
                [$id_workplace, $idReservation, $date_start, $date_end, $date_start, $date_end]
            );
        } else {
            $countFreeDate = DB::select(
                'SELECT count(id) as count FROM reservation WHERE id_workplace = ? AND ((? BETWEEN datetime_start AND datetime_end) OR (? BETWEEN datetime_start AND datetime_end) OR (? < datetime_start and ? > datetime_end))',
                [$id_workplace, $date_start, $date_end, $date_start, $date_end]
            );
        }

        if ((int) $countFreeDate[0]->count === 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check free data for Person
     *
     * @param int $id_person
     * @param $date_start
     * @param $date_end
     * @param int|null $idReservation
     * @return bool
     */
    private function checkFreeDataPerson(int $id_person, $date_start, $date_end, int $idReservation = null)
    {

        if ($idReservation !== null) {
            $countFreeDate = DB::select(
                'SELECT count(id) as count FROM reservation WHERE id_person = ? AND id != ? AND (( ? BETWEEN datetime_start AND datetime_end) OR ( ? BETWEEN datetime_start AND datetime_end) OR ( ? < datetime_start and ? > datetime_end))',
                [$id_person, $idReservation, $date_start, $date_end, $date_start, $date_end]
            );
        } else {
            $countFreeDate = DB::select(
                'SELECT count(id) as count FROM reservation WHERE id_person = ? AND (( ? BETWEEN datetime_start AND datetime_end) OR ( ? BETWEEN datetime_start AND datetime_end) OR ( ? < datetime_start and ? > datetime_end))',
                [$id_person, $date_start, $date_end, $date_start, $date_end]
            );
        }

        if ((int) $countFreeDate[0]->count === 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Edit Reservation
     *
     * @param ValidationReservation $request
     * @return JsonResponse
     */
    public function edit(ValidationReservation $request)
    {

        if ($this->checkModelsExist(
            $request->get('id_person'),
            $request->get('id_workplace')
        )
        ) {
            $reservation = ModelReservation::where('id', $request->get('id'));

            if ($reservation !== null and $reservation->count() === 1) {
                $date_start = $this->changeDateFormat($request->get('datetime_start'));
                $date_end = $this->changeDateFormat($request->get('datetime_end'));

                $dataStartActually = $reservation->get(['datetime_start', 'datetime_end']);
                $dataStartActually = $dataStartActually->toArray();

                if ($dataStartActually[0]['datetime_start'] !== $date_start
                    or $dataStartActually[0]['datetime_end'] !== $date_end
                ) {
                    // Date changed -> validiation

                    if (!$this->checkDateExpired($date_start, $date_end)) {
                        return response()->json(
                            ['messages' => ['Podana data już się przedawniła']
                            ],
                            422
                        );
                    }

                    if (!$this->checkDateLogic($date_start, $date_end)) {
                        return response()->json(
                            ['messages' => ['Data początkowa nie może być większa od daty końcowej']
                            ],
                            422
                        );
                    }

                    if (!$this->checkFreeDateWorkplace($request->get('id_workplace'), $date_start, $date_end, $request->get('id'))) {
                        return response()->json(
                            ['messages' => ['Miejsce pracy w wybranym terminie jest już zajęte']
                            ],
                            422
                        );
                    }

                    if (!$this->checkFreeDataPerson($request->get('id_person'), $date_start, $date_end, $request->get('id'))) {
                        return response()->json(
                            ['messages' => ['Ta osoba rezerwuje już miejsce w tym terminie']
                            ],
                            422
                        );
                    }
                }

                $reservation->update([
                    'id_person' => $request->get('id_person'),
                    'id_workplace' => $request->get('id_workplace'),
                    'datetime_start' => $date_start,
                    'datetime_end' => $date_end
                ]);

                return response()->json(null, 200);
            } else {
                return response()->json(['messages' => ['Błąd : nie można edytować danej rezerwacji'] ], 422);
            }
        } else {
            return response()->json(['messages' => ['Podana Osoba lub Miejsce pracy nie istnieje']], 422);
        }
    }

    /**
     * Get one Reservation
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id)
    {

        $reservation = DB::select('SELECT id, id_person, id_workplace, DATE_FORMAT(datetime_start,\'%Y-%m-%d %H:%i\') as datetime_start, DATE_FORMAT(datetime_end,\'%Y-%m-%d %H:%i\') as datetime_end FROM reservation WHERE id = ?', [$id]);

        if ($reservation !== null and count($reservation) === 1) {
            return  response()->json($reservation, 200);
        } else {
            return response()->json(['messages' => ['Błąd : Rezerwacja nie istnieje']], 422);
        }
    }

    /**
     * Get all Reservations
     *
     * @return JsonResponse
     */
    public function getAll()
    {

        $responseData = DB::select('SELECT r.id, CONCAT(p.name, \' \', p.surname) as person, w.mark as workplace, DATE_FORMAT(r.datetime_start,\'%Y-%m-%d %H:%i\') as datetime_start, DATE_FORMAT(r.datetime_end,\'%Y-%m-%d %H:%i\') as datetime_end FROM reservation as r, person as p, workplace as w WHERE r.id_person = p.id AND r.id_workplace = w.id');

        return response()->json($responseData, 200);
    }

    /**
     * Get data form
     *
     * @return JsonResponse
     */
    public function getDataForm()
    {

        $response = [];

        $allPersons = DB::select('SELECT id, Concat(name, \' \', surname) as name FROM person');
        $response['persons'] = $allPersons;

        $allWorkplaces = ModelWorkplace::all(['id', 'mark']);
        $response['workplaces'] = $allWorkplaces;

        $tempEquipments = [];
        foreach ($allWorkplaces as $workplace) {
            $equipments = ModelEquipment::where('id_workplace', $workplace->id);
            $tempEquipments[$workplace->id] = $equipments->get(['mark']);
        }

        $response['equipments'] = $tempEquipments;

        return response()->json($response, 200);
    }

    /**
     * Delete Reservation
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {

        $reservation = ModelReservation::where('id', $id);
        if ($reservation !== null and $reservation->count() === 1) {
            // reservation exist
            if ($reservation->delete()) {
                return response()->json(null, 200);
            } else {
                return response()->json(
                    ['messages' => ['Błąd usuwania miejsca pracy']
                    ],
                    422
                );
            }
        } else {
            return response()->json(
                ['messages' => ['Błąd usuwania miejsca pracy : dane miejsce pracy nie istnieje']
                ],
                422
            );
        }
    }
}
