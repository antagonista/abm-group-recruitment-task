<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Requests\Person as ValidationPerson;
use App\Http\Requests\PersonEdit as ValidationPersonEdit;
use App\Person as ModelPerson;
use App\Reservation as ModelReservation;

class PersonController extends Controller
{
    /**
     * Add new Person
     *
     * @param ValidationPerson $request
     * @return JsonResponse
     */
    public function add(ValidationPerson $request)
    {

        $person = new ModelPerson($request->all());

        if ($person->save()) {
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => ['Błąd przy zapisywaniu']], 422);
        }
    }

    /**
     * Edit person
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(ValidationPersonEdit $request)
    {

        $requestData = $request->all();

        $person = ModelPerson::where('id', $requestData['id']);

        if ($person !== null and $person->count() === 1) {
            $personActually = $person->get()->toArray();

            if ($personActually[0]['email'] !== $requestData['email']) {
                // Checking unique e-mail address when change

                $count = ModelPerson::where('email', $requestData['email'])
                    ->where('id', '!=', $personActually[0]['id'])
                    ->count();

                if ($count !== 0) {
                    return response()->json(['messages' => ['Podany adres e-m@il jest juz używany']], 422);
                }
            }

            $person->update([
                'name' => $requestData['name'],
                'surname' => $requestData['surname'],
                'phone_number' => $requestData['phone_number'],
                'email' => $requestData['email'],
                'description' => $requestData['description'],
            ]);
            return response()->json(null, 200);
        } else {
            return response()->json(['messages' => 'Błąd : nie można edytować danej osoby'], 422);
        }
    }

    /**
     * Get one Person
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id)
    {

        $person = ModelPerson::where('id', $id)
            ->get(['name', 'surname', 'phone_number', 'email', 'description']);

        if ($person !== null and $person->count() === 1) {
            return  response()->json($person, 200);
        } else {
            return response()->json(['messages' => ['Błąd : Dany użytkownik nie istnieje']], 422);
        }
    }

    /**
     * Get all Persons
     *
     * @return JsonResponse
     */
    public function getAll()
    {
        $allPersons = ModelPerson::all([
            'id',
            'name',
            'surname',
            'phone_number',
            'email',
            'description'
        ]);
        return response()->json($allPersons->all(), 200);
    }

    /**
     * Delete person by id
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {

        $person = ModelPerson::where('id', $id);
        if ($person !== null and $person->count() === 1) {
            // Person exist
            if ($person->delete()) {
                // Delete related reservations
                ModelReservation::where('id_person', $id)->delete();

                return response()->json(null, 200);
            } else {
                return response()->json(['messages' => ['Błąd usuwania użytkownika']], 422);
            }
        } else {
            return response()->json(['messages' => ['Błąd usuwania użytkownika : dany użytkownik nie istnieje']], 422);
        }
    }
}
