<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator as Validator;

class Reservation extends FormVueRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id_person' => [
                'required',
                'integer',
            ],
            'id_workplace' => [
                'required',
                'integer',
            ],
            'datetime_start' => [
                'required',
                'date',
            ],
            'datetime_end' => [
                'required',
                'date',
                'after:start'
            ]
        ];
    }
}
