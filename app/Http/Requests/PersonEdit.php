<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator as Validator;

class PersonEdit extends FormVueRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:24',
                'min:3'
            ],
            'surname' => [
                'required',
                'max:24',
                'min:3'
            ],
            'phone_number' => [
                'required',
                'size:9',
            ],
            'email' => [
                'required',
                'email',
                'max:230'
            ],
            'description' => [
                'max:255'
            ]
        ];
    }
}
