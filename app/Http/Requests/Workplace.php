<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator as Validator;

class Workplace extends FormVueRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'mark' => [
                'required',
                'max:25',
                'min:1',
                'unique:workplace'
            ],
            'description' => [
                'max:255'
            ]
        ];
    }
}
