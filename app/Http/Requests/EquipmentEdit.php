<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator as Validator;

class EquipmentEdit extends FormVueRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id_workplace' => [
                'integer',
                'nullable'
            ],
            'type' => [
                'required',
                'max:25',
                'min:1'
            ],
            'model' => [
                'required',
                'max:25',
                'min:1'
            ],
            'mark' => [
                'required',
                'max:25',
                'min:1'
            ],
            'purchase_year' => [
                'required',
                'integer'
            ],
            'value' => [
                'required',
                'numeric',
                'regex:/^[0-9]+[\.]?[0-9]{0,2}$/'
            ],
            'description' => [
                'max:255'
            ]
        ];
    }
}
