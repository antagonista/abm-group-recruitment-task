<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator as Validator;

class FormVueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Error Validation response
     *
     * @param Validator $validator
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {

        // Prepare error messages response
        $tempError = [];
        foreach ($validator->errors()->all() as $errorMsg) {
            $tempError[] = $errorMsg;
        }

        // Prepare error response
        $response = response()->json([
            'messages' => $tempError
        ], 422);

        throw new ValidationException($validator, $response);
    }
}
