import Vue from 'vue'
import DatetimePicker from 'vuetify-datetime-picker'

import Vuetify, {
    VApp,
    VAppBar,
    VNavigationDrawer,
    VToolbar,
    VToolbarTitle,
    VContainer,
    VContent,
    VRow,
    VCol,
    VFooter,
    VTooltip,
    VBtn,
    VIcon,
    VList,
    VListItem,
    VListItemAction,
    VListItemIcon,
    VListItemContent,
    VListItemTitle,
    VAppBarNavIcon,
    VForm,
    VTextField,
    VTextarea,
    VAlert,
    VSimpleTable,
    VSelect,
    VDatePicker,
    VTimePicker,
    VDialog,
    VCard,
    VCardText,
    VTabs,
    VTab,
    VTabItem,
    VSpacer,
} from "vuetify/lib";
import 'vuetify/dist/vuetify.min.css'


Vue.use(Vuetify, {
    components: {
        VApp,
        VAppBar,
        VNavigationDrawer,
        VToolbar,
        VToolbarTitle,
        VContainer,
        VContent,
        VRow,
        VCol,
        VFooter,
        VTooltip,
        VBtn,
        VIcon,
        VList,
        VListItem,
        VListItemAction,
        VListItemIcon,
        VListItemContent,
        VListItemTitle,
        VAppBarNavIcon,
        VForm,
        VTextField,
        VTextarea,
        VAlert,
        VSimpleTable,
        VSelect,
        VDatePicker,
        VTimePicker,
        VDialog,
        VCard,
        VCardText,
        VTabs,
        VTab,
        VTabItem,
        VSpacer,
    }
})


const opts = {}

export default new Vuetify(opts)
