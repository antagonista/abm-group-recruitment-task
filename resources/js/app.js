require('./bootstrap');

import Vue from 'vue'
import vuetify from "@/js/plugins/vuetify";
import axios from 'axios'
import VueAxios from 'vue-axios'


Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = '/api/'



import Route from '@/js/routes.js'
import App from '@/js/views/App'

import DatetimePicker from 'vuetify-datetime-picker'
import '../../node_modules/vuetify-datetime-picker/src/stylus/main.styl'
Vue.use(DatetimePicker)

const app = new Vue({
    el: '#app',
    vuetify,
    router: Route,
    render: h=> h(App)
})

export default app
