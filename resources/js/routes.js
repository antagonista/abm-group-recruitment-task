import Vue from 'vue'
import VueRouter from "vue-router"

// Pages
import Reservations from "@/js/pages/Reservations"
import Persons from "@/js/pages/Persons"
import Equipment from "@/js/pages/Equipment"
import Workplace from "@/js/pages/Workplace"

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Reservations
        },
        {
            path: '/persons',
            name: 'persons',
            component: Persons
        },
        {
            path: '/equipment',
            name: 'equipment',
            component: Equipment
        },
        {
            path: '/workplace',
            name: 'workplace',
            component: Workplace
        }
    ]
})

export default router
